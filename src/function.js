const progressBar = document.getElementById('progressBar');
const totalHeight = document.body.scrollHeight - window.innerHeight;

window.onscroll = () => {
    const progressHeight = (window.pageYOffset / totalHeight) * 100;
    progressBar.style.height = progressHeight + "%";
}